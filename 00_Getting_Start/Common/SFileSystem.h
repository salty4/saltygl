#pragma once
#include <string>
#include <cstdlib>

static const char* logl_root = "../../../..";
static const char* glsl_path = GLSL_PATH;
static const char* res_path = RES_PATH;

class SFileSystem
{
private:
    typedef std::string(*Builder) (const std::string& path);

public:
    static std::string getPath(const std::string& path)
    {
        static std::string(*pathBuilder)(std::string const&) = getPathBuilder();
        return (*pathBuilder)(path);
    }

    static std::string getShaderPath(const std::string& path)
    {
        return std::string(glsl_path) + path;
    }

    static std::string getResPath(const std::string& path)
    {
        return std::string(res_path) + path;
    }

private:
    static std::string const& getRoot()
    {
        static char const* envRoot = getenv("LOGL_ROOT_PATH");
        static char const* givenRoot = (envRoot != nullptr ? envRoot : logl_root);
        static std::string root = (givenRoot != nullptr ? givenRoot : "");
        return root;
    }

    //static std::string(*foo (std::string const &)) getPathBuilder()
    static Builder getPathBuilder()
    {
        if (getRoot() != "")
            return &SFileSystem::getPathRelativeRoot;
        else
            return &SFileSystem::getPathRelativeBinary;
    }

    static std::string getPathRelativeRoot(const std::string& path)
    {
        return getRoot() + std::string("/") + path;
    }

    static std::string getPathRelativeBinary(const std::string& path)
    {
        return "../../../" + path;
    }


};